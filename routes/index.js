const express = require('express');
const router = express.Router();
const sqlite3 = require('sqlite3').verbose();
const bodyParser = require('body-parser');
const path = require('path');

const dbPath = path.resolve(__dirname, '../database.db');

// Create a new SQLite database connection
const db = new sqlite3.Database(dbPath);
const jsonParser = bodyParser.json();

// Create the "dataTable" table if it does not exist
db.run(`CREATE TABLE IF NOT EXISTS "dataTable" (
  id INTEGER PRIMARY KEY,
  lastName TEXT,
  firstName TEXT,
  age INTEGER
)`, (err) => {
  if (err) {
    console.error('Error creating table:', err);
  } else {
    console.log('Table created or already exists');
  }
});

// Define a route to handle the user's input
router.post('/send-data', jsonParser, (req, res) => {
  const jsonData = req.body;

  if (!jsonData.rows || !Array.isArray(jsonData.rows)) {
    console.log('Invalid JSON format:', jsonData);
    return res.status(400).json({ error: 'Invalid JSON format' });
  }

  db.serialize(() => {
    jsonData.rows.forEach((row) => {
      const placeholders = ['?', '?', '?'].join(',');
      const values = [row.lastName, row.firstName, row.age];

      db.run(`INSERT INTO "dataTable" ("lastName", "firstName", "age") VALUES (${placeholders})`, values, (err) => {
        if (err) {
          console.error('Error inserting data into the database:', err);
        }
      });
    });

    console.log('Data inserted successfully:', jsonData.rows);
    res.status(200).json({ message: 'Data inserted successfully' });
  });
});

// Define a route to retrieve and send data from the 'dataTable' table
router.get('/get-data', (req, res) => {
  // Query the 'dataTable' table and convert the result to JSON
  db.all('SELECT * FROM "dataTable"', (err, rows) => {
    if (err) {
      console.error('Error querying the database:', err);
      return res.status(500).json({ error: 'Internal Server Error' });
    }

    console.log('Data queried successfully:', rows);
    // Send the data as JSON in the response
    res.json(rows);
  });
});

module.exports = router;
